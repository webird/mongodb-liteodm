<?php

namespace WeBird\LiteOdm;

use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\Reader;
use MongoDB\Database;
use WeBird\LiteOdm\Entity\Configuration\ConfigurationBuilder;
use WeBird\LiteOdm\Exceptions\DatabaseNotRegisteredException;

class ServiceContainer
{
    /** @var static */
    protected static $instance = null;
    /** @var Reader */
    protected $reader;
    /** @var ConfigurationBuilder */
    protected $configurationBuilder;
    /** @var array|Database[] */
    protected $databases = [];

    public function __construct(Reader $reader, $databases = [])
    {
        $this->reader = $reader;
        $this->initializeAnnotationReader();

        $this->configurationBuilder = new ConfigurationBuilder();

        $this->databases = array_filter($databases, function ($db) {
            return $db instanceof Database;
        });


        static::$instance = $this;
    }

    protected function initializeAnnotationReader()
    {
        AnnotationRegistry::registerAutoloadNamespace(
            'WeBird\\LiteOdm\\Entity\\Annotation',
            [__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..']
        );
        AnnotationRegistry::registerAutoloadNamespace(
            'WeBird\\LiteOdm\\Repository\\Annotation',
            [__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..']
        );
    }

    /**
     * @return ServiceContainer
     */
    public static function instance()
    {
        if (null === static::$instance) {
            throw new \RuntimeException('LiteODM Service container have to be initialized at first');
        }
        return static::$instance;
    }

    /**
     * @return Reader
     */
    public function getAnnotationReader()
    {
        return $this->reader;
    }

    /**
     * @return ConfigurationBuilder
     */
    public function getConfigurationBuilder()
    {
        return $this->configurationBuilder;
    }

    /**
     * @param string $key
     * @return Database
     */
    public function getDatabase($key = 'default')
    {
        if (empty($this->databases[$key])) {
            throw new DatabaseNotRegisteredException(
                sprintf(
                    'Database with key \'%s\' was not registered',
                    $key
                )
            );
        }

        return $this->databases[$key];
    }

    public function hasDatabase($key)
    {
        return isset($this->databases[$key]);
    }
}
