<?php

namespace WeBird\LiteOdm\Repository\Command;

class UpdateFieldCommand
{
    const NOT_IMPORTANT = '__not_important_at_update';

    /** @var mixed */
    private $previousValue = self::NOT_IMPORTANT;

    /** @var mixed */
    private $newValue;

    /** @var string */
    private $field;

    /**
     * UpdateFieldCommand constructor.
     * @param string $field
     * @param mixed $value
     * @param mixed $previousValue
     */
    public function __construct(string $field, $value, $previousValue = self::NOT_IMPORTANT)
    {
        $this->newValue = $value;
        $this->previousValue = $previousValue;
        $this->field = $field;
    }

    /**
     * @return null|mixed
     */
    public function getPreviousValue()
    {
        return $this->previousValue;
    }

    /**
     * @return mixed
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * @return bool
     */
    public function haveToControlValueOnUpdate(): bool
    {
        return $this->previousValue !== self::NOT_IMPORTANT;
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->field;
    }

}
