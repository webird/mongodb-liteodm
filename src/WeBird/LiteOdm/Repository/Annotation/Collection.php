<?php

namespace WeBird\LiteOdm\Repository\Annotation;

/**
 * Class Collection
 * @package WeBird\LiteOdm\Repository\Annotation
 * @Annotation
 * @Target({"CLASS"})
 */
class Collection
{
    public $database = 'default';

    public $collection;
}