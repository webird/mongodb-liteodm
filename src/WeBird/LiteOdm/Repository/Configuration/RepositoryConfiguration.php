<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 18.03.16
 * Time: 21:33
 */

namespace WeBird\LiteOdm\Repository\Configuration;


use MongoDB\Collection;
use MongoDB\Database;

class RepositoryConfiguration
{
    /**
     * @var Collection
     */
    private $collection;
    /**
     * @var Database
     */
    private $database;

    /**
     * RepositoryConfiguration constructor.
     * @param Collection $collection
     * @param Database $database
     */
    public function __construct(Collection $collection, Database $database)
    {
        $this->collection = $collection;
        $this->database = $database;
    }

    /**
     * @return Collection
     */
    public function getCollection(): Collection
    {
        return $this->collection;
    }

    /**
     * @return Database
     */
    public function getDatabase(): Database
    {
        return $this->database;
    }
}