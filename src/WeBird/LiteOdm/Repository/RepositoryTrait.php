<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 18.03.16
 * Time: 21:16
 */

namespace WeBird\LiteOdm\Repository;

use AppBundle\Repository\AddressRepository;
use MapKernel\Model\Address;
use MongoDB\BSON\Javascript;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\Persistable;
use MongoDB\BulkWriteResult;
use MongoDB\Driver\Cursor;
use MongoDB\Driver\Exception\RuntimeException;
use MongoDB\Model\BSONDocument;
use WeBird\LiteOdm\Entity\EntityInterface;
use WeBird\LiteOdm\Entity\IdGeneratorInterface;
use WeBird\LiteOdm\Entity\PartiallyUpdatableInterface;
use WeBird\LiteOdm\Exceptions\InvalidConfigurationProvidedException;
use WeBird\LiteOdm\Repository\Annotation\Collection;
use WeBird\LiteOdm\Repository\Command\UpdateFieldCommand;
use WeBird\LiteOdm\Repository\Configuration\RepositoryConfiguration;
use WeBird\LiteOdm\ServiceContainer;

trait RepositoryTrait
{
    /** @var RepositoryConfiguration */
    private static $_odm_configuration = null;

    /**
     * @param $documents EntityInterface|EntityInterface[]
     * @return BulkWriteResult
     */
    public function save($documents) :BulkWriteResult
    {
        $collection = $this->readConfiguration()->getCollection();

        if (!is_array($documents)) {
            $documents = [$documents];
        }

        $documentsForInsert = [];
        $operations = [];
        foreach ($documents as $document) {
            if ($document instanceof PartiallyUpdatableInterface && $changes = $document->getChanges()) {
                $operations[] = $this->buildOperationFromChanges($document, $changes);
            } elseif ($document->getId() === null) {
                $documentsForInsert[] = $document;
                if ($document instanceof IdGeneratorInterface) {
                    $document->generateId();
                }
                $operations[] = ['insertOne' => [$document]];
            } else {
                $operations[] = ['replaceOne' => [['_id' => $document->getId()], $document, ['upsert' => true]]];
            }
        }

        $result = $collection->bulkWrite($operations);
        $ids = $result->getInsertedIds();

        foreach ($documentsForInsert as $i => $document) {
            isset($ids[$i]) && $document->setId($ids[$i]);
        }

        return $result;
    }

    /**
     * @return RepositoryConfiguration
     */
    private function readConfiguration(): RepositoryConfiguration
    {
        if (method_exists($this, 'doRepositoryConfigure')) {
            $configuration = $this->{'doRepositoryConfigure'}();
            if (!$configuration || !($configuration instanceof RepositoryConfiguration)) {
                throw new InvalidConfigurationProvidedException(
                    'Invalid configuration has been provided by \'doRepositoryConfigure\' method'
                );
            }
            return $configuration;
        }

        if (self::$_odm_configuration !== null) {
            return self::$_odm_configuration;
        }

        $annotationReader = $this->getMongoServiceContainer()->getAnnotationReader();
        $reflection = new \ReflectionClass(self::class);

        /** @var Collection $collectionAnnotation */
        $collectionAnnotation = $annotationReader->getClassAnnotation($reflection, Collection::class);

        $db = $this->getMongoServiceContainer()->getDatabase($collectionAnnotation->database);
        $collection = $db->selectCollection($collectionAnnotation->collection);

        self::$_odm_configuration = new RepositoryConfiguration($collection, $db);

        return self::$_odm_configuration;
    }

    /**
     * @return ServiceContainer
     */
    private function getMongoServiceContainer(): ServiceContainer
    {
        return ServiceContainer::instance();
    }

    /**
     * @param EntityInterface $document
     * @param UpdateFieldCommand[] $changes
     * @return array
     */
    protected function buildOperationFromChanges(EntityInterface $document, array $changes)
    {
        $filter = [
            '_id' => $document->getId()
        ];
        $update = [];
        foreach ($changes as $change) {
            $update[$change->getFieldName()] = $change->getNewValue();
            $change->haveToControlValueOnUpdate()
            && $filter[$change->getFieldName()] = $change->getPreviousValue();
        }

        return ['updateOne' => [$filter, ['$set' => $update], ['multi' => false]]];
    }

    /**
     * @param string|ObjectID $id
     * @return Persistable|EntityInterface|null
     */
    public function findOneById($id)
    {
        return $this->findOne(['_id' => $id]);
    }

    /**
     * @return \MongoDB\Collection
     */
    public function getCollection(): \MongoDB\Collection
    {
        return $this->readConfiguration()->getCollection();
    }

    /**
     * @param array $query
     * @param array $options
     * @return null|object|EntityInterface
     */
    protected function findOne(array $query = [], array $options = [])
    {
        $collection = $this->readConfiguration()->getCollection();

        return $collection->findOne($query, $options);
    }

    /**
     * @param array $query
     * @param array $options
     * @return \MongoDB\Driver\Cursor
     */
    protected function find(array $query = [], array $options = []): Cursor
    {
        $collection = $this->readConfiguration()->getCollection();

        return $collection->find($query, $options);
    }

    /**
     * @param array $query
     * @param array $options
     * @return int
     */
    protected function count(array $query = [], array $options = []) :int
    {
        $collection = $this->readConfiguration()->getCollection();

        return $collection->count($query, $options);
    }

    /**
     * @param array $pipeline
     * @param array $options
     * @return \Traversable
     */
    protected function aggregate(array $pipeline, array $options = [])
    {
        $collection = $this->readConfiguration()->getCollection();

        return $collection->aggregate($pipeline, $options);
    }

    /**
     * @param Javascript $map
     * @param Javascript $reduce
     * @param array $query
     * @param array $options
     * @return array
     */
    protected function doMapReduce(Javascript $map, Javascript $reduce, array $query = [], array $options = [])
    {
        $db = $this->readConfiguration()->getDatabase();

        $collection = $this->readConfiguration()->getCollection();
        $tmpCollection = uniqid('mapReduce_' . $collection->getCollectionName());

        $command = [
            'mapReduce' => $collection->getCollectionName(),
            'map' => $map,
            'reduce' => $reduce,
            'out' => $tmpCollection
        ];

        if ($query) {
            $command['query'] = $query;
        }

        /** @var BSONDocument|null $result */
        $result = current($db->command($command + $options)->toArray());
        if (!$result || empty($result->ok) || empty($result->result) || $result->ok != 1) {
            throw new RuntimeException('Unable to execute mapReduce');
        }

        $tmpCollection = $result->result;

        $data = $db->selectCollection($tmpCollection)->find([])->toArray();

        $db->dropCollection($tmpCollection);

        return $data;
    }

    /**
     * @param string|ObjectID $id
     * @return \MongoDB\DeleteResult
     */
    protected function removeOne($id)
    {
        $collection = $this->readConfiguration()->getCollection();

        return $collection->deleteOne(['_id' => $id]);
    }
}
