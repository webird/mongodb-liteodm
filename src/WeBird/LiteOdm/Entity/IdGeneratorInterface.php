<?php

namespace WeBird\LiteOdm\Entity;

interface IdGeneratorInterface
{
    public function generateId(): string;
}