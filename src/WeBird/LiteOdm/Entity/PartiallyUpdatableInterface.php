<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 18.03.16
 * Time: 21:58
 */

namespace WeBird\LiteOdm\Entity;

use WeBird\LiteOdm\Repository\Command\UpdateFieldCommand;

interface PartiallyUpdatableInterface extends EntityInterface
{

    /**
     * @return UpdateFieldCommand[]
     */
    public function getChanges(): array;

}