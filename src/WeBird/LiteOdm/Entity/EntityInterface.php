<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 18.03.16
 * Time: 22:06
 */

namespace WeBird\LiteOdm\Entity;

use MongoDB\BSON\Persistable;

interface EntityInterface extends Persistable
{

    /**
     * @return \MongoDB\BSON\ObjectID|string
     */
    public function getId();

}
