<?php

namespace WeBird\LiteOdm\Entity\Configuration;

class FieldConfiguration
{

    /** @var string */
    private $type;

    /** @var string|null */
    private $setter;

    /** @var string|null */
    private $getter;

    /** @var mixed */
    private $default;

    public function __construct($type, $default = null, $setter = null, $getter = null)
    {
        $this->type = $type;
        $this->setter = $setter;
        $this->getter = $getter;
        $this->default = $default;
    }

    /**
     * @return null|string
     */
    public function getGetter()
    {
        return $this->getter;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return null|string
     */
    public function getSetter()
    {
        return $this->setter;
    }

    /**
     * @return bool
     */
    public function haveToBeAddedIfNull()
    {
        return $this->type != 'id';
    }

    /**
     * @return mixed
     */
    public function getDefault()
    {
        return $this->default;
    }
}
