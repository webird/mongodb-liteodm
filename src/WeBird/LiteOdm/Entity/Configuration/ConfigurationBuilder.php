<?php

namespace WeBird\LiteOdm\Entity\Configuration;


use WeBird\LiteOdm\Entity\Annotation\Field;

class ConfigurationBuilder
{
    /** @var FieldConfiguration[] */
    protected $fields = [];

    public function configureFieldWithAnnotation($field, Field $annotation)
    {
        $this->configureField($field, $annotation->type, $annotation->default, $annotation->setter, $annotation->getter);
        return $this;
    }

    public function configureField($field, $type, $default = null, $setter = null, $getter = null)
    {
        $this->fields[$field] = new FieldConfiguration($type, $default, $setter, $getter);
        return $this;
    }

    public function build()
    {
        return new EntityConfiguration($this->fields);
    }

}