<?php

namespace WeBird\LiteOdm\Entity\Configuration;


class EntityConfiguration
{
    /**
     * @var FieldConfiguration[]
     */
    protected $fields = [];

    /**
     * EntityConfiguration constructor.
     * @param FieldConfiguration[] $fields
     */
    public function __construct(array $fields = [])
    {
        $this->fields = $fields;
    }

    /**
     * @param string $name
     * @return FieldConfiguration
     */
    public function getField(string $name)
    {
        if (empty($this->fields[$name])) {
            throw new \RuntimeException(sprintf('Field \'%s\' was not configured', $name));
        }

        return $this->fields[$name];
    }

    /**
     * @return FieldConfiguration[]
     */
    public function getFields()
    {
        return $this->fields;
    }
}
