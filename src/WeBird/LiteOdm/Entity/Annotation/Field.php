<?php

namespace WeBird\LiteOdm\Entity\Annotation;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Field
{
    /** @var string */
    public $type;

    /** @var string */
    public $setter;

    /** @var string */
    public $getter;

    /** @var mixed */
    public $default;

}
