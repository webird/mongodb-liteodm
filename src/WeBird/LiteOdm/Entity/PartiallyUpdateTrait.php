<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 18.03.16
 * Time: 22:18
 */

namespace WeBird\LiteOdm\Entity;

use MongoDB\BSON\UTCDateTime;
use WeBird\LiteOdm\Repository\Command\UpdateFieldCommand;

trait PartiallyUpdateTrait
{
    /** @var UpdateFieldCommand[] */
    protected $changes = [];

    /**
     * @return UpdateFieldCommand[]
     */
    public function getChanges(): array
    {
        return $this->changes;
    }

    /**
     * @param UpdateFieldCommand[] $changeCommands
     */
    protected function setChanges(array $changeCommands)
    {
        $this->changes = $changeCommands;
    }

    protected function addFieldValueChange(
        string $fieldName,
        $newValue,
        $previousValue = UpdateFieldCommand::NOT_IMPORTANT
    ) {
        $prepareValue = function ($value) {
            return is_object($value) && !($value instanceof UTCDateTime)? clone $value : $value;
        };

        $this->changes[] = new UpdateFieldCommand(
            $fieldName,
            $prepareValue($newValue),
            $prepareValue($previousValue)
        );
    }
}