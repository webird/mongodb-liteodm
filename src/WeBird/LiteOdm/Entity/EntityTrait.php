<?php

namespace WeBird\LiteOdm\Entity;


use WeBird\LiteOdm\Entity\Annotation\Field;
use WeBird\LiteOdm\Entity\Configuration\ConfigurationBuilder;
use WeBird\LiteOdm\Entity\Configuration\EntityConfiguration;
use WeBird\LiteOdm\ServiceContainer;

trait EntityTrait
{
    /** @var EntityConfiguration */
    protected static $_odm_configuration = null;

    /**
     * Provides an array or document to serialize as BSON
     * Called during serialization of the object to BSON. The method must return an array or stdClass.
     * Root documents (e.g. a MongoDB\BSON\Serializable passed to MongoDB\BSON\fromPHP()) will always be
     * serialized as a BSON document.
     * For field values, associative arrays and stdClass instances will be serialized as a BSON document
     * and sequential arrays (i.e. sequential, numeric indexes starting at 0) will be serialized as a BSON array.
     * @link http://php.net/manual/en/mongodb-bson-serializable.bsonserialize.php
     * @return array|object An array or stdClass to be serialized as a BSON array or document.
     */
    public function bsonSerialize()
    {
        $configuration = $this->readConfiguration();

        $document = [];

        foreach ($configuration->getFields() as $fieldName => $field) {
            if ($field->getGetter() && method_exists($this, $field->getGetter())) {
                $document[$fieldName] = $this->{$field->getGetter()}();
            } else {
                $document[$fieldName] = $this->{$fieldName};
            }
            if (!$field->haveToBeAddedIfNull() && $document[$fieldName] === null) {
                unset($document[$fieldName]);
            }
        }

        return $document;
    }

    private function readConfiguration()
    {
        if (static::$_odm_configuration !== null) {
            return static::$_odm_configuration;
        }

        if (method_exists($this, 'doEntityConfigure')) {
            $configuration = static::{'doEntityConfigure'}();
            if (!($configuration instanceof EntityConfiguration)) {
                throw new \RuntimeException("Wrong configuration has been provided by 'doODMConfigure' method");
            }
        }

        $ref = new \ReflectionClass($this);
        $annotationReader = $this->getMongoServiceContainer()->getAnnotationReader();

        $configurationBuilder = new ConfigurationBuilder();

        foreach ($ref->getProperties() as $reflectionProperty) {
            /** @var Field $field */
            $field = $annotationReader->getPropertyAnnotation($reflectionProperty, Field::class);

            if ($field) {
                $configurationBuilder->configureFieldWithAnnotation($reflectionProperty->getName(), $field);
            }
        }

        static::$_odm_configuration = $configurationBuilder->build();
        return static::$_odm_configuration;
    }

    /**
     * @return ServiceContainer
     */
    private function getMongoServiceContainer()
    {
        return ServiceContainer::instance();
    }

    /**
     * Constructs the object from a BSON array or document
     * Called during unserialization of the object from BSON.
     * The properties of the BSON array or document will be passed to the method as an array.
     * @link http://php.net/manual/en/mongodb-bson-unserializable.bsonunserialize.php
     * @param array $document Properties within the BSON array or document.
     */
    public function bsonUnserialize(array $document)
    {
        $configuration = $this->readConfiguration();

        foreach ($configuration->getFields() as $fieldName => $field) {
            if ($field->getGetter() && method_exists($this, $field->getSetter())) {
                $this->{$field->getSetter()}($document[$fieldName]);
            } else {
                $this->{$fieldName} = $document[$fieldName] ?? $field->getDefault();
            }
        }
    }

}
