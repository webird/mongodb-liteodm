<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 18.03.16
 * Time: 21:40
 */

namespace WeBird\LiteOdm\Exceptions;

use MongoDB\Exception\RuntimeException;

class InvalidConfigurationProvidedException extends RuntimeException
{

}
