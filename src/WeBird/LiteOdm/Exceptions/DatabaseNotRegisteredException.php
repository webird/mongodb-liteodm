<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 18.03.16
 * Time: 21:28
 */

namespace WeBird\LiteOdm\Exceptions;

use MongoDB\Exception\InvalidArgumentException;

class DatabaseNotRegisteredException extends InvalidArgumentException
{
}
